function ok = calibrate_Eyetracker_cogent(inkey,current_buffer)

% ok = calibrate_Eyetracker_cogent(inkey,current_buffer)
%
% inkey can be 'c' to calibrate, 'v' to validate, 'd' to drift correct (or
%    empty to allow free selection)
% 
% ok returns -1 if failed, 0 if Escape pressed, 1 if successful
%  calibrates eyetracker via cogent. needs cogent to be running already.
%
% 

ok = -1;
el = EyelinkInitDefaults;

global cogent;
keymap = getkeymap;

if nargin<2
    current_buffer = 1;
end
clearpict(current_buffer);

if nargin<1
    n = 2;
    while n > 1
        fprintf('EYELINK: Press c to calibrate, v to validate, d to drift correct, Esc to skip\n');
        [keyout,~,n] = waitkeydown(180000,[keymap.C keymap.V keymap.D keymap.Escape]);
    end
    if n==0
        warning('timeout - waited more than 3 minutes for experimenter to tell eyelink what to do!');
        keyout = keymap.Escape;
    end
    
    switch keyout
        case keymap.Escape %breakout of function
            ok = 0;
            Eyelink('SendKeyButton', 27, 0, el.KB_PRESS );
            return;
        case keymap.V
            inkey = 'v';
        case keymap.C
            inkey = 'c';
        case keymap.D
            inkey = 'd';
        otherwise
            error('bug in script - unrecognised waitkeydown output...');
    end
end

%% 1. make sure eyetracker is set up and calibrated
if ~Eyelink('IsConnected')
    if Eyelink( 'Initialize' ) ~= 0; 
        warning('unable to form connection with eyetracker');
        return; 
    end % open a connection to the eyetracker PC
end

Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');
poss_res = [640 480;...
    800 600;...
    1024 768;...
    1152 864;...
    1280 1024;...
    1600 1200];
SCREEN_RES = cogent.display.res;

Eyelink('Command', sprintf('screen_pixel_coords = %0.1f, %0.1f, %0.1f, %0.1f',...
    0,0,poss_res(SCREEN_RES,1),poss_res(SCREEN_RES,2)));
    
switch lower(inkey)
    case {'c' 'v'}
        preparestring('Follow the dots!',current_buffer,0,0);t = 300;
    case 'd'
        preparestring('Fixate!',current_buffer,0,0);t = 300;
end
drawpict(current_buffer);wait(t);clearpict(current_buffer)

%% 2. set up calibration info

%link_sample_data
Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0,poss_res(SCREEN_RES,1),poss_res(SCREEN_RES,2));
Eyelink('command', 'calibration_type = HV9');
Eyelink('command', 'generate_default_targets = YES');

% Eyelink('command','calibration_samples = 6');
% Eyelink('command','calibration_sequence = 0,1,2,3,4,5');
% Eyelink('command','calibration_targets = %d,%d %d,%d %d,%d %d,%d %d,%d',...
%     round(width/2),round(height/2), round(width/2),round(height*0.2),...
%     round(  width/2),round(height - height*0.2), round( width*0.2),round(height/2),round(  width - width*0.2),round(height/2 ));
% Eyelink('command','validation_samples = 5');
% Eyelink('command','validation_sequence = 0,1,2,3,4,5');
% Eyelink('command','validation_targets = %d,%d %d,%d %d,%d %d,%d %d,%d',...round(width/2),round(height/2), round(width/2),round(height*0.2),...
%     round(  width/2),round(height - height*0.2), round( width*0.2), ...
% round(height/2),round(  width - width*0.2),round(height/2 ));

Eyelink('command', 'saccade_velocity_threshold = 35');
Eyelink('command', 'saccade_acceleration_threshold = 9500');

[v,vs] = Eyelink('GetTrackerVersion');
fprintf('Running experiment on a ''%s'' tracker.\n', vs );
vsn = regexp(vs,'\d','match'); % wont work on EL I
if isempty(vsn)
    eyelinkI = 1;
else
    eyelinkI = 0;
end

if v == 3 && str2double(vsn{1}) == 4 % if EL 1000 and tracker version 4.xx
   
    % remote mode possible add HTARGET ( head target)
    Eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT');
    Eyelink('command', 'file_sample_data  = LEFT,RIGHT,GAZE,HREF,AREA,GAZERES,STATUS,INPUT,HTARGET');
    % set link data (used for gaze cursor)
    Eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,FIXUPDATE,INPUT');
    Eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,INPUT,HTARGET');
else
    Eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT');
    Eyelink('command', 'file_sample_data  = LEFT,RIGHT,GAZE,HREF,AREA,GAZERES,STATUS,INPUT');
    % set link data (used for gaze cursor)
    Eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,FIXUPDATE,INPUT');
    Eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,INPUT');
end

Eyelink('command', 'button_function 5 "accept_target_fixation"'); %don't know what this does.

if v == 3
    % set pupil Tracking model in camera setup screen
    % no = centroid. yes = ellipse
    Eyelink('command', 'use_ellipse_fitter = no');
    % set sample rate in camera setup screen
    Eyelink('command', 'sample_rate = %d',1000);
end

% % if desktop mount and tracker version is 4.2 or later change
% % illumination
% twropts = {'TOWER','MPRIM','MIRROR','BLRR','MLRR'};
% % query tracker for mount type using elcl_select_configuration variable
% [result,reply]=Eyelink('ReadFromTracker','elcl_select_configuration');

%% 3. Run calibration

Eyelink('Command', 'heuristic_filter = ON');
Eyelink( 'StartSetup' );		% start setup mode
Eyelink( 'WaitForModeReady', el.waitformodereadytime );  % time for mode change

%gets rid of key presses stored in Eyelink:
key=1;
while key~= 0
    key=EyelinkGetKey(el);		% dump old keys stored in Eyelink
end

%to start calibration:
Eyelink('SendKeyButton', double(inkey), 0, el.KB_PRESS );
Eyelink( 'WaitForModeReady', el.waitformodereadytime );  % time for mode change

%run through calibration
stop = 0;
while stop==0 & bitand(Eyelink( 'CurrentMode'), el.IN_SETUP_MODE)

    i=Eyelink( 'CurrentMode');
    if ~Eyelink( 'IsConnected' ) stop=1; break; end;
    
    %to current check target location
    Eyelink( 'WaitForModeReady', el.waitformodereadytime );  % time for mode change
    [result, tx, ty]= Eyelink( 'TargetCheck')
    
    if ~result
        stop = 1;
    elseif bitand(i, el.IN_TARGET_MODE)			% calibrate, validate, etc: show targets
        [tx,ty] = convert_EL_to_cogent(tx,ty);
        
        cgsetsprite(current_buffer);
        cgellipse(tx,ty,10,10,[1 1 1],1,1);
        cgellipse(tx,ty,2,2,[0 0 0],1,1);
        drawpict(current_buffer);
    
    elseif bitand(i, el.IN_IMAGE_MODE)		% display image until we're back
        error('Using ''image mode'' not yet implemented in cogent Eyelink toolbox');
%         % 		fprintf ('%s\n', 'EyelinkDoTrackerSetup: in ''ImageModeDisplay''' );
%         if Eyelink ('ImageModeDisplay')==el.TERMINATE_KEY
%             result=el.TERMINATE_KEY;
%             return;    % breakout key pressed
%         else
%             EyelinkClearCalDisplay(el);	% setup_cal_display()
%         end
    else 
        stop = 1;
    end
    
    %get keypress from cogent keyboard
    n = 2;
    while n > 1
        [keyout,~,n] = waitkeydown(250,[keymap.Space keymap.Escape keymap.Return ...
            keymap.M keymap.A keymap.BackSpace]);
        if n==0 %check every 250 ms if target has changed position, due to auto-triggering
            [result, tx2, ty2]= Eyelink( 'TargetCheck');
            [tx2,ty2] = convert_EL_to_cogent(tx2,ty2);
            if ~result||tx~=tx2||ty~=ty2 %potential autotrigger or change on eyelink computer
                keyout = 'tmp';
            else
                n = 2; %no change return back to waitkeydown
            end
        end
    end
    
    %handle buttonpress:
    switch keyout
        case keymap.Space %accept calibration point/switch to manual
            Eyelink('SendKeyButton', 32, 0, el.KB_PRESS );
        case keymap.Escape %terminate current calibration
            Eyelink('SendKeyButton', 27, 0, el.KB_PRESS );
        case keymap.Return %accept calibration point/switch to manual
            Eyelink('SendKeyButton', 13, 0, el.KB_PRESS );
        case keymap.M %manual triggering
            Eyelink('SendKeyButton', 109, 0, el.KB_PRESS );
        case keymap.A %auto triggering
            Eyelink('SendKeyButton', 97, 0, el.KB_PRESS );
        case keymap.BackSpace %repeat previous location
            Eyelink('SendKeyButton', 8, 0, el.KB_PRESS );
    end
    Eyelink( 'WaitForModeReady', el.waitformodereadytime );  % time for mode change
    clearpict(current_buffer);
    
    
    %gets rid of key presses stored in Eyelink:
    key=1;
    while key~= 0
        key=EyelinkGetKey(el);		% dump old keys stored in Eyelink
    end
end

%clear display
drawpict(current_buffer);

ok = 1;
