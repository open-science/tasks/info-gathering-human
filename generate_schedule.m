function sched = generate_schedule(condin,nTr)

if condin==5 %alternate between adding and multiplying every 10 trial
    order = randperm(4);
    cond = order(1);
    count = 1;
else
    cond = condin; 
end

i=1;
while i<=nTr
    sched(i).nTurns     = 4;
    sched(i).nOpt       = 2;
    sched(i).nAttr      = 2;
    sched(i).value      = randi(10,2,2);
    sched(i).uncovered  = zeros(2,2);
    sched(i).canrespond = ones(4,1);
    sched(i).availstim  = 'images/blueback.jpg';
    sched(i).unavailstim= 'images/redback.jpg';
    
    sched(i).cost       = nan(4,2,2);
    tmp = randperm(4);
    sched(i).cost(1,tmp(1)) = 0;
    sched(i).cost(2,tmp(2)) = 10;
    sched(i).cost(3,:)      = 15;
    sched(i).cost(4,:)      = 20;
    
    %load in stimuli
    sched(i).stim{1,1} = ['images/hearts/' num2str(sched(i).value(1,1)) 'H.jpg'];
    sched(i).stim{1,2} = ['images/diamonds/' num2str(sched(i).value(1,2)) 'D.jpg'];
    sched(i).stim{2,1} = ['images/clubs/' num2str(sched(i).value(2,1)) 'C.jpg'];
    sched(i).stim{2,2} = ['images/spades/' num2str(sched(i).value(2,2)) 'S.jpg'];
    
    switch cond
        case 1 %add
            sched(i).reward     = ((sum(sched(i).value,2)==max(sum(sched(i).value,2)))-0.5);
            sched(i).reward(sched(i).reward>0) = 60; %win trials
            sched(i).reward(sched(i).reward<0) = -50; %loss trials
            sched(i).rule       = 'add - find biggest';
            sched(i).rulecode   = 1;
        case 2 %multiply
            sched(i).reward     = ((prod(sched(i).value,2)==max(prod(sched(i).value,2)))-0.5);
            sched(i).reward(sched(i).reward>0) = 60; %win trials
            sched(i).reward(sched(i).reward<0) = -50; %loss trials
            sched(i).rule       = 'multiply - find biggest';
            sched(i).rulecode   = 2;
        case 3 %add
            sched(i).reward     = ((sum(sched(i).value,2)==min(sum(sched(i).value,2)))-0.5);
            sched(i).reward(sched(i).reward>0) = 60; %win trials
            sched(i).reward(sched(i).reward<0) = -50; %loss trials
            sched(i).rule       = 'add - find smallest';
            sched(i).rulecode   = 3;
        case 4 %multiply
            sched(i).reward     = ((prod(sched(i).value,2)==min(prod(sched(i).value,2)))-0.5);
            sched(i).reward(sched(i).reward>0) = 60; %win trials
            sched(i).reward(sched(i).reward<0) = -50; %loss trials
            sched(i).rule       = 'multiply - find smallest';
            sched(i).rulecode   = 4;
        otherwise
            error('unsupported condition');
    end
    if length(unique(sched(i).reward))==1 %quits trials
        %do nothing - we're not having quits trials anymore
        %sched(i).reward(:) = nan;
    else
        i=i+1;
    end
    
    if condin==5&&rem(i,10)==0
        count = count+1; if count>4; count = 1; end
        cond = order(count);
    end
end

