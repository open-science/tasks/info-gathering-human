%% INFORMATION GATHERING TASK

SEEDVAL=floor(rem(now*10000,1)*10000); %use clock for seed

% v1 - LH 01/03/2013
rand('seed',SEEDVAL); % reseed random number generator
complete = 0;

%% EXPERIMENTAL SETUP

%constants - check these before every run
CONDITION       = 5;        % 1 = addBIG; 2= multiplyBIG; 3 = addSMALL; 4 = multSMALL; 5 = interleaved
EYETRACKING     = 0;        % eyetracking?
EXPCONTROL      = 0;        % wait for experimenter input at various points in expt.?
DISPLAY         = 0;        % 0 for window, 1 for whole screen (see config_display, below)
NUM_TRIALS      = 100;

% rootdir - change for each computer

rootdir = 'D:\My Documents\home\projects\info_gathering_human';


%% 1. ask for subject ID and setup logfile

lfOK = 0; %logfile check flag
if CONDITION==99
    outfile = sprintf('%s/data/logfiles/debug.mat',rootdir);
else
    while ~lfOK
        subjID = input('Subject ID: ','s');
        runno = str2double(input('Run: ','s'));
        
        switch CONDITION
            case 0
                outfile = sprintf('%s/data/logfiles/%s_main_%0.0f.mat',rootdir,subjID,runno);
            case 1
                outfile = sprintf('%s/data/logfiles/%s_add_%0.0f.mat',rootdir,subjID,runno);
            case 2
                outfile = sprintf('%s/data/logfiles/%s_mult_%0.0f.mat',rootdir,subjID,runno);
            case 3
                outfile = sprintf('%s/data/logfiles/%s_addSMALL_%0.0f.mat',rootdir,subjID,runno);
            case 4
                outfile = sprintf('%s/data/logfiles/%s_multSMALL_%0.0f.mat',rootdir,subjID,runno);
            case 5
                outfile = sprintf('%s/data/logfiles/%s_mixed_%0.0f.mat',rootdir,subjID,runno);
            otherwise
                error('unrecognised condition number');
        end
        lfOK = ~exist(outfile,'file')||strcmp(subjID,'test')||CONDITION==99;
        if ~lfOK
            fprintf('logfile already exists for subject %s!\n',subjID);
        end
    end
    
    if isempty(subjID);
        error('Empty subjID');
    end
end
clear lfOK

%% 2. configure cogent

config_mouse;
config_keyboard;
switch DISPLAY
    case 0 %window
        config_display(0,3,[0 0 0],[1 1 1],'Arial',40,5); %resolution:1280x1024
    case 1 %whole-screen
        config_display(1,3,[0 0 0],[1 1 1],'Arial',40,5); %resolution:1280x1024
end

if EYETRACKING
    if EXPCONTROL
        fprintf('EXPERIMENTER: Check eyetracker is accurately calibrated\n');
        pause;
    end
    if eyelink( 'Initialize' ) ~= 0; return; end % open a connection to the eyetracker PC
    ELfname = [subjID num2str(runno)];
    eyelink( 'Openfile', ELfname )                % open a file, ELfname, on the eyetracker PC
end

start_cogent;
keymap = getkeymap;

%% 3. generate schedule, set timing and button info

switch CONDITION
    case {[1] [2] [3] [4] [5]}
        sched = generate_schedule(CONDITION,NUM_TRIALS);
    case 99
        load('dummytrial.mat'); %load in single trial schedule
    otherwise
        error('not yet implemented this condition');
end

timings.ITItime     = 500;
timings.choicetime  = 10000;
timings.selecttime  = 750;
timings.uncovertime = 1500;
timings.ruletime    = 1750;
timings.feedbacktime= 2000;
timings.dwelltime   = 300;

buttons.opt = [keymap.A; keymap.D];
buttons.pic = [keymap.Pad7 keymap.Pad1; keymap.Pad9 keymap.Pad3];

%% 4. preload pictures?

%% 5. initialise variables

%% 6. introduce expt.

if CONDITION~=99
    clearpict( 1 );
    preparestring( 'Game will begin shortly', 1 )
    drawpict( 1 );
    wait(3000);
    clearpict( 1 );
    drawpict( 1 );
end

%% 7. run task
roundscores = [];

for t = 1:length(sched) %loop over trials
    if t==1||~strcmp(sched(t).rule,sched(t-1).rule)
        cG = 1;preparestring(upper(sched(t).rule),cG);
        drawpict(cG);wait(2000);clearpict(cG);
    end
        
    %TODO move this into generate_schedule?
    schedt = sched(t);
    schedt.loc(:,1,1) = [-250 200];
    schedt.loc(:,1,2) = [-250 -100];
    schedt.loc(:,2,1) = [250  200];
    schedt.loc(:,2,2) = [250 -100];
    schedt.ruleloc(:,1)=[-250 -250];
    schedt.ruleloc(:,2)=[250  -250];
    schedt.rulestrloc(:,1)=[-250 50];
    schedt.rulestrloc(:,2)=[250  50];    
    schedt.rewloc(:,1)=[-250 -280];
    schedt.rewloc(:,2)=[250  -280];
    schedt.selr(:,1) =  [-250 25 210 650];
    schedt.selr(:,2) =  [250 25 210 650];
    
    [chosen(t), chose_to_reveal{t}, turndur{t}, noresponse(t), rewout(t), sumcost(t), outcome(t)] =...
        run_IG_trial(schedt,buttons,timings);
    
    save(outfile);
   
    if mod(t,40)==0
        tmp = outcome(t-39:t); tmp(isnan(tmp))=[]; roundscores(end+1) = sum(tmp)/100; clear tmp;
        clearpict( 1 );
        if roundscores(end)>0
            setforecolour(0,1,0);
        else
            setforecolour(1,0,0);
        end
        preparestring(sprintf('In the last 40 trials, you made a total of �%0.2f',roundscores(end)),1);
        drawpict( 1 );
        setforecolour(1,1,1);
        wait(3000);
        if (length(sched)-t)>39
            clearpict( 1 );
            preparestring( 'Take a break! The experiment will restart shortly.', 1 );
            drawpict( 1 );
            wait(20000);
            clearpict( 1 );
            preparestring( 'Whenever you''re ready, press any button to restart the experiment.', 1 );
            drawpict( 1 );
            waitkeydown(Inf);
            clearpict( 1 );
            drawpict( 1 );
        end
    end
end

%% 8. cleanup and save
complete = 1;
save(outfile);

%% 9. exit

cgshut;
stop_cogent;

roundscores
