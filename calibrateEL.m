function calibrateEL(window,screenNumber,dummymode,bgc,ctc)

%calibrate Eyelink eyetracker

if nargin < 3
    dummymode = 0;
end

% Background color
if nargin < 4
    bgc = [0 0 0];
end

% Target color
if nargin < 5
    ctc = [255,255,255];
end

% Provide Eyelink with details about the graphics environment
% and perform some initializations. The information is returned
% in a structure that also contains useful defaults
% and control codes (e.g. tracker state bit and Eyelink key values).

el=EyelinkInitDefaults(window);

el.msgfontcolour            = WhiteIndex(el.window);
el.imgtitlecolour           = WhiteIndex(el.window);
el.targetbeep               = 0;
el.calibrationtargetcolour  = ctc;
el.calibrationtargetsize    = 1;
el.calibrationtargetwidth   = 0.5;
el.displayCalResults        = 1;
el.eyeimgsize               = 30;
el.imgtitlefontsize         = 15;
el.backgroundcolour         = bgc;
EyelinkUpdateDefaults(el);

% Initialization of the connection with the Eyelink Gazetracker.
% exit program if this fails.
if ~EyelinkInit(dummymode)
    fprintf('Eyelink Init aborted.\n');
    return;
end

[width, height]=Screen('WindowSize', screenNumber);


% make sure we're still connected.
if Eyelink('IsConnected')~=1 && ~dummymode
    fprintf('Not connected. exiting');
    return;
end

Eyelink('command', 'generate_default_targets = YES');



% modify calibration and validation target locations

Eyelink('command','calibration_samples = 6');
Eyelink('command','calibration_sequence = 0,1,2,3,4,5');
Eyelink('command','calibration_targets = %d,%d %d,%d %d,%d %d,%d %d,%d',...
    round(width/2),round(height/2),round(width/2),round(height*0.2), ...
    round(width/2),round(height-height*0.2),round(width*0.2),...
    round(height/2),round(width - width*0.2),round(height/2));
Eyelink('command','validation_samples = 5');
Eyelink('command','validation_sequence = 0,1,2,3,4,5');
Eyelink('command','validation_targets = %d,%d %d,%d %d,%d %d,%d %d,%d',...
    round(width/2),round(height/2),round(width/2),round(height*0.2), ...
    round(width/2),round(height-height*0.2),round(width*0.2),...
    round(height/2),round(width - width*0.2),round(height/2));

% allow to use the big button on the eyelink gamepad to accept the
% calibration/drift correction target

Eyelink('command', 'button_function 5 "accept_target_fixation"');
[~, reply]=Eyelink('ReadFromTracker','enable_automatic_calibration');

if reply % reply = 1
    fprintf('Automatic sequencing ON.\n');
else
    fprintf('Automatic sequencing OFF.\n');
end

% enter Eyetracker camera setup mode, calibration and validation
EyelinkDoTrackerSetup(el);

end
