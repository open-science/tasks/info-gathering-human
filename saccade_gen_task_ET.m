function saccade_gen_task_ET

%% main experiment settings

DISPLAY         = 1;        % 0 for window, 1 for whole screen 1, 2 for whole screen 2 (see config_display, below)
SCREEN_RES      = 3;        %(1=640x480, 2=800x600, 3=1024x768, 4=1152x864, 5=1280x1024, 6=1600x1200)
SCANNING        = 1;
rootdir = 'C:\Users\nskandal\Desktop\info_gathering_human';

%% screen locations

loc(1,:) = [-250 150];
loc(2,:) = [-250 -150];
loc(3,:) = [250  150];
loc(4,:) = [250 -150];
loc(5,:) = [0 0];

nL = length(loc);

%% program settings

nSacc = 15; %number of saccades for each location

include_all = 1; %include saccades from all locations to all other locations

ok = 0;runs = 0;
while ~ok
    %% set up count matrix
    
    %Smat is saccade matrix of how many saccades to count from one location (rows) to
    %another (columns)
    if include_all
        Smat = (ones(nL) - eye(nL))*nSacc;
    end
    
    nSaccTot = sum(Smat(:)); %total number of saccades
    
    %% create dotOrder list - list of locations to visit
    
    [~,i] = max(sum(Smat,1)); %starting location
    dotOrder = nan(nSaccTot+1,1);
    dotOrder(1) = i;
    for s = 1:nSaccTot
        %determine next saccade location
        tmp = max(Smat(i,:));
        ind = find(Smat(i,:)==tmp);ind = ind(randperm(length(ind)));
        j = ind(1);
        
        %add to list
        Smat(i,j) = Smat(i,j) - 1;
        dotOrder(s+1) = j;
        i = j; clear j
    end
    
    if all(Smat(:)==0)
        ok = 1;
    else
        runs = runs+1;
        if runs>1000
            error('unable to find satisfactory dot presentation order...');
        end
    end
    
end

%% 1. ask for subject ID and setup logfile

lfOK = 0; %logfile check flag
while ~lfOK
    subjID = input('Subject ID: ','s');
    runno = str2double(input('Run: ','s'));
    
    outfile = sprintf('%s/data/logfiles_sacc_gen/%s_main_%0.0f.mat',rootdir,subjID,runno);
    
    if exist(outfile,'file')&&~strcmp(subjID,'test');
        fprintf('logfile already exists for subject %s!\n',subjID);
    elseif length(subjID)>3
        fprintf('subjID must not exceed 3 characters!');
    else
        lfOK = 1;
    end
end

if isempty(subjID);
    error('Empty subjID');
end

clear lfOK

%% 2.set up cogent

config_mouse;
config_keyboard;
config_display(DISPLAY,SCREEN_RES,[0 0 0],[1 1 1],'Arial',40,5); 

if eyelink( 'Initialize' ) ~= 0; return; end % open a connection to the eyetracker PC
ELfname = ['sg_' subjID num2str(runno)];
Eyelink( 'Openfile', ELfname )                % open a file, ELfname, on the eyetracker PC
Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');

start_cogent;
keymap = getkeymap;

%% 3. calibrate and start eyetracker

runET = 1;
while runET
    sound(sin(0:100*pi/1000:100*pi)); %play beep to alert experimenter.
    runET = calibrate_Eyetracker_cogent;
end
poss_res = [640 480;...
    800 600;...
    1024 768;...
    1152 864;...
    1280 1024;...
    1600 1200]; %cogent screen resolutions
Eyelink('Command', sprintf('screen_pixel_coords = %0.1f, %0.1f, %0.1f, %0.1f',...
    0,0,poss_res(SCREEN_RES,1),poss_res(SCREEN_RES,2)));
ret_val = eyelink( 'StartRecording' );        % start recording (to ELfile)

%% 4. introduce expt.

clearpict( 1 );
preparestring( 'Follow the dots when they change from grey to white!', 1 )
drawpict( 1 );
wait(3000);
clearpict( 1 );
drawpict( 1 );

%% 5. run task
timelog = struct('time',[],'code',[],'event',[]); timelog(1) = [];

for s = 1:nSaccTot
    tag_trial(s);
    
    sacc_ind = (dotOrder(s)-1)*5+dotOrder(s+1);
    
    %draw grey dot
    gazeloc_from = [loc(dotOrder(s),1) loc(dotOrder(s),2) 100];
    gazeloc_to   = [loc(dotOrder(s+1),1) loc(dotOrder(s+1),2) 100];
    disp(s);
    
    cG = 1;
    
    settextstyle('Arial',40);setforecolour(1, 1, 1);
    
    ok = 0;
    while ok==0
        preparestring(sprintf('%c',7),cG,gazeloc_from(1),gazeloc_from(2));       
        [~,~,noresponse,~,timelog(end+1)] = ...
            present_graphic_with_saccade_contingent_response(cG,1500,[],200,gazeloc_from,1,SCANNING,sacc_ind,'FROMDOT ON');
        if ~noresponse %fixated OK.
            ok = 1;
        else %perform drift correction
            Eyelink('StopRecording');
            sound(sin(0:100*pi/1000:100*pi)); %play beep to alert experimenter.
            calibrate_Eyetracker_cogent('d');
            Eyelink('StartRecording');
        end
    end
    
    ok = 0;
    while ok==0
        presdur(s) = 300+round(rand*400);
        setforecolour(0.5, 0.5, 0.5);
        preparestring(sprintf('%c',7),cG,gazeloc_to(1),gazeloc_to(2));
        
        setforecolour(1, 1, 1);
        preparestring(sprintf('%c',7),cG,gazeloc_from(1),gazeloc_from(2));
        
        [~,~,noresponse,~,timelog(end+1)] = ...
            present_graphic_with_saccade_contingent_response(cG,1500,[],300+rand(400),gazeloc_from,1,SCANNING,sacc_ind+25,'TODOT ON');
                if ~noresponse %fixated OK.
            ok = 1;
        else %perform drift correction
            Eyelink('StopRecording');
            sound(sin(0:100*pi/1000:100*pi)); %play beep to alert experimenter.
            calibrate_Eyetracker_cogent('d');
            Eyelink('StartRecording');
        end
    end
    wait(presdur);
    clearpict(cG);
    
    ok = 0;
    while ok==0
        setforecolour(1, 1, 1);
        preparestring(sprintf('%c',7),cG,gazeloc_to(1),gazeloc_to(2));       
        [~,~,noresponse,~,timelog(end+1)] = ...
            present_graphic_with_saccade_contingent_response(cG,1500,[],200,gazeloc_to,1,SCANNING,sacc_ind+50,'TODOT MOVE');
        if ~noresponse %fixated OK.
            ok = 1;
        else %perform drift correction
            Eyelink('StopRecording');
            sound(sin(0:100*pi/1000:100*pi)); %play beep to alert experimenter.
            calibrate_Eyetracker_cogent('d');
            Eyelink('StartRecording');
        end
    end
    
    if mod(s,80)==0
        if (nSaccTot-s)>79
            clearpict( 1 );
            preparestring( 'Take a break! The experiment will restart shortly.', 1 );
            drawpict( 1 );
            wait(10000);
            clearpict( 1 );
            preparestring( 'Whenever you''re ready, press any button to restart the experiment.', 1 );
            drawpict( 1 );
            waitkeydown(Inf);
            clearpict( 1 );
            drawpict( 1 );
            Eyelink('StopRecording');
            runET = 1;
            while runET
                sound(sin(0:100*pi/1000:100*pi)); %play beep to alert experimenter.
                runET = calibrate_Eyetracker_cogent;
            end
            Eyelink('StartRecording');
        end
    end
    
    save(outfile);
end



    
end

%% SACCADE-CONTINGENT RESPONSE PRESENTATION FUNCTION
function [choseopt,RT,noresponse,out_modality,timelog] = ...
    present_graphic_with_saccade_contingent_response(cG,tmax,hotkeys,dwelltime,gazelocs,draw_dots,scanning,portcode,eventname);

DRAW_TARGETS = 0;

hkvalid = hotkeys(hotkeys~=0);

drawpict(cG);
if nargin>6
    if scanning;
        outportb(888,portcode);
    end
    timelog.time = time;
    timelog.code  = portcode;
    timelog.event = eventname;
    if scanning;
        wait(10); outportb(888,0);
        tmax = tmax - 10;
    end
else
    timelog = [];
end

[choseopt,RT,out_modality] = gaze_resp_choice(gazelocs,hotkeys,dwelltime,tmax,cG,draw_dots,DRAW_TARGETS);

clearpict(cG);

if isnan(choseopt) %no response
    noresponse = 1;
else
    noresponse = 0;
end
end
