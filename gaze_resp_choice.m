function [out,rt,out_modality] = gaze_resp_choice(gazelocs,hotkeys,dwelltime,timeout,current_buffer,drawsacc,draw_targets);

t0 = time; %time at beginning of function call
if nargin<7
    draw_targets = 0;
end
if nargin<6
    drawsacc = 0;
end
if nargin<5
    current_buffer = nan;
end
if nargin<4
    timeout = Inf;
end
if nargin<3
    dwelltime = 1; %ms
end
if nargin<2
    hotkeys = [];
end
if nargin<1
    error('not enough input arguments');
end

if ~isempty(gazelocs)
    if size(gazelocs,2) ~= 3
        error('gazelocs must be n*3 matrix (x,y,diameter)');
    end
end

%% initialise output arguments

out = nan;
rt  = nan;
out_modality = 0;

%% loop until timeout

global cogent;

readkeys;
logkeys;
el=EyelinkInitDefaults;
eye_used = -1;

%draw targets, if requested
if draw_targets
    cgsetsprite(current_buffer);
    for i = 1:size(gazelocs,1)
        cgellipse(gazelocs(i,1),gazelocs(i,2),gazelocs(i,3)*2,gazelocs(i,3)*2,[1 1 1]);
    end
    drawpict(current_buffer);
end

while (time-t0)<timeout && isnan(out)
    %% 1. check if a long enough fixation has been made
    if ~isempty(gazelocs)
        
        %a. get eyetracker data
        if Eyelink('NewFloatSampleAvailable') > 0
            % get the sample in the form of an event structure
            evt = Eyelink( 'NewestFloatSample');
            if eye_used ~= -1 % do we know which eye to use yet?
                % if we do, get current gaze position from sample
                x = evt.gx(eye_used+1); % +1 as we're accessing MATLAB array
                y = evt.gy(eye_used+1);
                if x==el.MISSING_DATA & y==el.MISSING_DATA
                    x = nan;
                    y = nan;
                end
            else % if we don't, first find eye that's being tracked
                eye_used = Eyelink('EyeAvailable'); % get eye that's tracked
                if eye_used == el.BINOCULAR; % if both eyes are tracked
                    eye_used = el.LEFT_EYE; % use left eye only (hack, for time being)
                end
                x = nan;
                y = nan;
            end
        end
        
        %b. convert to cogent coordinates
        [x,y] = convert_EL_to_cogent(x,y);
        
        %c. see if x,y are within targets
        current_target = classify_xy(gazelocs,x,y);
        if isempty(current_target)
            previous_target = [];
        elseif isempty(previous_target)||current_target~=previous_target
            targetin = time;
            previous_target = current_target;
        elseif (time-targetin)>dwelltime
            out = current_target;
            rt  = targetin-t0; %time of first acquiring target is counted as reaction time
            out_modality = 1;
        end
        
    end
    
    %% 2. check if one of the hotkeys has been depressed (n.b. this takes priority over saccade)
    if ~isempty(hotkeys)
        readkeys;
        logkeys;
        
        %128 specifies it must be a down press rather than a release of the key:
        index = find( cogent.keyboard.value == 128 & ismember(cogent.keyboard.id,hotkeys));
        if length(index)==1 %restrict so only one key may be depressed
            out = cogent.keyboard.id( index );
            rt  = cogent.keyboard.time( index );
            out_modality = 2;
        end
        
    end
    
    %% 3. draw saccade, if requested
    if drawsacc&&exist('x','var')&&exist('y','var')
        if ~isempty(current_target)
            setforecolour(1,1,1); %can change this if you want saccade dots to be different colour when target is selected
        else
            setforecolour(1,1,1);
        end
        preparestring('.',current_buffer,x,y);
        drawpict(current_buffer);
    end
    
    %% 4. wait 1ms
    cogsleep(1);
end
setforecolour(1,1,1);

function out = classify_xy(gazelocs,x,y)

out = [];
xdist = gazelocs(:,1) - x;
ydist = gazelocs(:,2) - y;
dist =  sqrt(xdist.^2 + ydist.^2);
[dist,ind] = sort(dist,'ascend');
for i = 1:size(gazelocs,1) %search through the targets, starting with the closest
    if dist(i)<gazelocs(ind(i),3)
        out = ind(i);
        return;
    end
end

