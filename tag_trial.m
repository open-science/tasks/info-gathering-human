function tag_trial(ct)

% tags trial ct with the following code:
% - 15ms delay
% - 10ms at code 201 (code begins)
% - 10ms delay
% - 10ms at v2 (100-199)
% - 10ms delay
% - 10ms at v1 (100-199)
% - 10ms delay
% - 10ms at code 202 (code ends)
% - 15ms delay
%
% to reconstruct ct, use formula ct = 100*(v2-100) + (v1-100);

if ct>9999
    error('tag_trial cannot handle trial integer greater than 9999');
end

v1 = 100 + rem(ct,100); %value of first code - 100 to 199 (for last two digits of number)
v2 = 100 + (ct-rem(ct,100))/100; %value of first code - 100 to 199 (for last two digits of number)

wait(15);

outportb(888,201);
wait(10);

outportb(888,0);
wait(10);

outportb(888,v2);
wait(10);

outportb(888,0);
wait(10);

outportb(888,v1);
wait(10);

outportb(888,0);
wait(10);

outportb(888,202);
wait(10);

outportb(888,0);
wait(15);