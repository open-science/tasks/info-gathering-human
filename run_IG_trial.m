function [chosen, chose_to_reveal, turndur, noresponse, rewout, sumcost, outcome] =...
    run_IG_trial(sched,buttons,timings)

% run_IG_trial(sched,buttons,timings)
%
% runs trial of information gathering experiment
%

% v1 - LH 01/03/2013

%% some constants - these could be changed to input arguments in future versions

WRITECOSTS = 1; %display written costs on each card when available?
CHOSECOLOUR=[0.3 0.3 0.3];
DISPLAY_ALL_FB = 0; %display feedback on each option?

%% load in info from input arguments

%trial schedule
if numel(sched)>1
    error('sched should be just a single-trial structure');
end
nT          = sched.nTurns;
nO          = sched.nOpt;
nA          = sched.nAttr;
cost        = sched.cost;       % cost for each turn, matrix dims nT * nO * nA; nan if unavailable
reward      = sched.reward;     % reward/punishment on each option, dims nO * 1 
value       = sched.value;      % value of item at each location, dims nO * nA
stim        = sched.stim;       % picture file at each location, nO * nA
rule        = sched.rule;       % rule that is implemented
rewloc      = sched.rewloc;     % locations in which to display information about reward, 2(x,y) * nO
ruleloc     = sched.ruleloc;    % locations in which to display information about rule, 2(x,y) * nO
rulestrloc  = sched.rulestrloc; % locations in which to display information about rule, 2(x,y) * nO
uncovered   = sched.uncovered;  % whether the options start out uncovered or not, nO * nA (1/0)
canrespond  = sched.canrespond; % whether the subject is allowed to choose at each turn, nT * 1
availstim   = sched.availstim;  % stimulus to display when option can be uncovered
unavailstim = sched.unavailstim;% stimulus to display when option cannot be uncovered
loc         = sched.loc;        % locations of stimuli in pixels, 2(x,y) * nO * nA 
selr        = sched.selr;       % selection rectangle height and width for each option 4(x,y,h,w) * nO
try coststim= sched.coststim;end% cost stimulus for each turn, cell array dims nT * nO * nA; nan if unavailable
try rewstim = sched.rewstim; end% reward stimulus for each option, cell array dims nO *1

% TODO check dimensions of inputs

optkey      = buttons.opt;      % active buttons for each option, nO*1
pickey      = buttons.pic;      % active buttons for each picture, nO*nA

ITItime     = timings.ITItime;  % duration of ITI
choicetime  = timings.choicetime;%time allowed for response, can be single number or nT * 1
if numel(choicetime)==1
    choicetime = repmat(choicetime,nT+1,1);
end
selecttime  = timings.selecttime;%time for which selected option is presented, with other options covered
uncovertime = timings.uncovertime;%time for which selected option is presented, with all other options covered
ruletime    = timings.ruletime; % time for which rule is presented
feedbacktime= timings.feedbacktime;%time for which feedback is presented

chosen = nan;
chose_to_reveal = nan;
turndur = nan;
noresponse = nan;
sumcost = nan;
rewout = nan;

%% 1. present intertrial interval

if ITItime>0
    cG = 1; cgsetsprite(cG);
    present_graphic(cG,ITItime);
end

%% 2. present decision phase, until decision is made (or timeout occurs)

chosen = 0;
noresponse = 0;
sumcost = 0;
turndur = nan(nT,1);

t = 1; %current turn

while ~chosen&~noresponse
    cG = 1;cgsetsprite(cG); %current graphic
    
    %a. draw picture
    for p = 1:numel(uncovered)
        if uncovered(p)                 %draw uncovered stimulus at current location
            loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
        elseif isnan(cost(t,p))         %picture is not available
            loadpict_mod(unavailstim,cG,loc(1,p),loc(2,p));
        else                            %picture is covered and available
            loadpict_mod(availstim,cG,loc(1,p),loc(2,p));
            if exist('coststim','var')    %overlay cost stimulus?
                loadpict_mod(coststim{t,p},cG,loc(1,p),loc(2,p));
            elseif WRITECOSTS
                cgrect(loc(1,p),loc(2,p),80,40,[0 0 0]);
                setforecolour(1,0,0);
                preparestring(sprintf('%0.0fp',cost(t,p)),cG,loc(1,p),loc(2,p));
                setforecolour(1,1,1);
            end
        end
    end
    
    %b. calculate permissable responses
    if t<=nT
        availpic = squeeze(~isnan(cost(t,:,:)))&~uncovered; %1 if available to uncover on this turn, 0 if not
        if canrespond(t) %allowed to make choice, as well as uncover, on this turn
            hotkeys = [optkey; pickey(availpic(:))];
        else             %only allowed to uncover on this turn
            hotkeys = pickey(availpic(:))';
        end
    else %all information has been uncovered - final turn
        hotkeys = optkey;
    end
    
        
    %c. present picture and await response
    [choseopt,turndur(t),noresponse] = present_graphic_with_response(cG,choicetime(t),hotkeys);
    
    
    
    %d. calculate what to do next
    if any(choseopt==optkey)                %MADE CHOICE...
        chose_to_reveal(t) = nan;
        chosen=find(choseopt==optkey);          %break from loop    
    elseif any(choseopt==pickey(:))         %UNCOVERED PICTURE...
        chose_to_reveal(t) = find(choseopt==pickey);
        uncovered(choseopt==pickey(:))=1;       %turn over picture in next turn
        sumcost = sumcost + cost(t,find(choseopt==pickey(:)));%add cost to sumcost
        t=t+1;                                  %move onto next turn
    elseif ~noresponse                      %MADE UNIDENTIFIED RESPONSE
        error('bug in code - response should either be noresponse, chose option, or turned over picture');
    end
end

%% 3. present selected option

if selecttime>0&&~noresponse
    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(uncovered)
        if uncovered(p)                 %draw uncovered stimulus at current location
            loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
        else
            loadpict_mod(unavailstim,cG,loc(1,p),loc(2,p));
        end
    end
    present_graphic(cG,selecttime);
end

if uncovertime>0&&~noresponse
    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(stim)
        loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
    end
    present_graphic(cG,uncovertime);
end

%% 4. do rule-based calculations and present, if required

%calculate values
switch rule(1:3)
    case 'add'
        for o = 1:nO;
            rulestr = '+';
            ruleout{o} = ['= ' num2str(sum(value(o,:)))];
        end
    case 'mul'
        for o = 1:nO;
            rulestr = 'X';
            ruleout{o} = ['= ' num2str(prod(value(o,:)))];
        end
end

%calculate reward strings
rewstr = cell(nO,1);
for o = 1:nO
    if isnan(reward(o))
        rewstr{o} = 'Quits';
    else
        rewstr{o} = sprintf('%0.0fp',reward(o));
    end
end

%present
if ruletime>0&~noresponse
    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(stim)
        loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
    end
    for o = 1:nO
        preparestring(rulestr,cG,rulestrloc(1,o),rulestrloc(2,o));
    end
    present_graphic(cG,ruletime*0.3);

    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(stim)
        loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
    end
    for o = 1:nO
        preparestring(rulestr,cG,rulestrloc(1,o),rulestrloc(2,o));
    end
    for o = 1:nO
        preparestring(ruleout{o},cG,ruleloc(1,o),ruleloc(2,o));
%         if (o==chosen||DISPLAY_ALL_FB)
%             if isnan(reward(o))
%                 setforecolour(1,1,0);
%             elseif reward(o)>0
%                 setforecolour(0,1,0);
%             elseif reward(o)<0
%                 setforecolour(1,0,0);
%             end
%             settextstyle('Arial',24);
%             preparestring(rewstr{o},cG,rewloc(1,o),rewloc(2,o));
%             settextstyle('Arial',40);
%             setforecolour(1,1,1);
%         end
    end
    
    present_graphic(cG,ruletime*0.7);
end

%% 5. calculate outcome and present, if required

%calculate outcome
if ~noresponse
    if isnan(reward(chosen))
        coststr = '';
    else
        coststr = sprintf('You paid %0.0fp',sumcost);
    end
    rewout = reward(chosen)
    outcome = rewout-sumcost;
    if isnan(reward(chosen))
        outstr = '';OScol = [0 0 0];
    elseif outcome>=0
        outstr = sprintf('Overall gain: �%0.2f',outcome/100); OScol = [0 1 0];
    else
        outstr = sprintf('Overall loss: �%0.2f',outcome/100); OScol = [1 0 0];
    end
else
    outcome = nan;
end

if ~noresponse
    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(stim) %uncovered stimuli
        loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
    end
    if exist('rewstim','var')
        for r = 1:numel(rewstim)
            if ~isempty(rewstim{r})
                loadpict_mod(rewstim{r},cG,selr(1,r),selr(2,r));
            end
        end
    end
    for o = 1:nO
        preparestring(rulestr,cG,rulestrloc(1,o),rulestrloc(2,o));
    end
    for o = 1:nO
        preparestring(ruleout{o},cG,ruleloc(1,o),ruleloc(2,o));
        if (o==chosen||DISPLAY_ALL_FB)
            if isnan(reward(o))
                setforecolour(1,1,0);
            elseif reward(o)>0
                setforecolour(0,1,0);
            elseif reward(o)<0
                setforecolour(1,0,0);
            end
            settextstyle('Arial',24);
            preparestring(rewstr{o},cG,rewloc(1,o),rewloc(2,o));
            settextstyle('Arial',40);
            setforecolour(1,1,1);
        end
    end
    settextstyle('Arial',30)
    setforecolour(1,0,0);preparestring(coststr,cG,0,15);
    setforecolour(OScol(1),OScol(2),OScol(3));preparestring(outstr,cG,0,-15);setforecolour(1,1,1);
    settextstyle('Arial',40)
    present_graphic(cG,feedbacktime);
end

%% 6. present hurry up screen, if subject didn't respond
if noresponse
    chosen = nan;
    cG = 2;cgsetsprite(cG);

    settextstyle('Arial',40);preparestring('Too slow!',cG);
    present_graphic(cG,1000);
end


end %end of main function



%% STIMULUS PRESENTATION FUNCTION
function present_graphic(cG,presdur)

drawpict(cG);
wait(presdur);
clearpict(cG);

end


%% RESPONSE PRESENTATION FUNCTION
function [choseopt,RT,noresponse] = present_graphic_with_response(cG,tmax,hotkeys)

hkvalid = hotkeys(hotkeys~=0);

drawpict(cG);
clearpict(cG);

n=2;
tin = time;
while n>1 %sometimes waitkeydown will erroneously return two numbers rather than one.
    [choseopt,tout,n] = waitkeydown(tmax,hkvalid);
end
RT = tout - tin;

if isempty(choseopt) %no response
    noresponse = 1;
    choseopt = nan;
    RT = nan;
else
    noresponse = 0;
end
end
