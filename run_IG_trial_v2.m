function [chosen, chose_to_reveal, turndur, noresponse, rewout, sumcost, outcome, timelog] =...
    run_IG_trial_v2(sched,buttons,timings,trind,scanning,PCs)

% run_IG_trial_v2(sched,buttons,timings)
%
% runs trial of information gathering experiment, with saccade contingent
% card turnover
%

% v2 - LH 14/05/2013

if nargin<4
    trind = 1;
end

%% some constants - these could be changed to input arguments in future versions

WRITECOSTS = 1; %display written costs on each card when available?
CHOSECOLOUR=[0.3 0.3 0.3];
DISPLAY_ALL_FB = 0; %display feedback on each option?
DRAW_DOTS = 1;

%% load in info from input arguments

%trial schedule
if numel(sched)>1
    error('sched should be just a single-trial structure');
end
nT          = sched.nTurns;
nO          = sched.nOpt;
nA          = sched.nAttr;
cost        = sched.cost;       % cost for each turn, matrix dims nT * nO * nA; nan if unavailable
reward      = sched.reward;     % reward/punishment on each option, dims nO * 1 
value       = sched.value;      % value of item at each location, dims nO * nA
stim        = sched.stim;       % picture file at each location, nO * nA
rule        = sched.rule;       % rule that is implemented
rewloc      = sched.rewloc;     % locations in which to display information about reward, 2(x,y) * nO
ruleloc     = sched.ruleloc;    % locations in which to display information about rule, 2(x,y) * nO
rulestrloc  = sched.rulestrloc; % locations in which to display information about rule, 2(x,y) * nO
rulecode    = sched.rulecode;   % code corresponding to rule used
uncovered   = sched.uncovered;  % whether the options start out uncovered or not, nO * nA (1/0)
canrespond  = sched.canrespond; % whether the subject is allowed to choose at each turn, nT * 1
availstim   = sched.availstim;  % stimulus to display when option can be uncovered
unavailstim = sched.unavailstim;% stimulus to display when option cannot be uncovered
loc         = sched.loc;        % locations of stimuli in pixels, 2(x,y) * nO * nA 
selr        = sched.selr;       % selection rectangle height and width for each option 4(x,y,h,w) * nO
try coststim= sched.coststim;end% cost stimulus for each turn, cell array dims nT * nO * nA; nan if unavailable
try rewstim = sched.rewstim; end% reward stimulus for each option, cell array dims nO *1

% TODO check dimensions of inputs

optkey      = buttons.opt;      % active buttons for each option, nO*1
pickey      = buttons.pic;      % active buttons for each picture, nO*nA

ITItime     = timings.ITItime;  % duration of ITI
choicetime  = timings.choicetime;%time allowed for response, can be single number or nT * 1
if numel(choicetime)==1
    choicetime = repmat(choicetime,nT+1,1);
end
selecttime  = timings.selecttime;%time for which selected option is presented, with other options covered
dwelltime   = timings.dwelltime; %time on which subject has to dwell before option is uncovered
dwelltime2  = timings.dwelltime2; %time on which subject has to dwell after option is uncovered
uncovertime = timings.uncovertime;%time for which selected option is presented, with all other options covered
ruletime    = timings.ruletime; % time for which rule is presented
feedbacktime= timings.feedbacktime;%time for which feedback is presented

chosen = nan;
chose_to_reveal = nan;
turndur = nan;
noresponse = nan;
sumcost = nan;
rewout = nan;

timelog = struct('time',[],'code',[],'event',[]); timelog(1) = [];

%% 1. present intertrial interval

if ITItime>0
    cG = 1; cgsetsprite(cG);
    timelog(end+1) = present_graphic(cG,ITItime,scanning,PCs.ITI,'ITI');
end

%% 1.1 Acquire fixation

%central fixation point
ok = 0;
settextstyle('Arial',40);
while ok==0
    preparestring(sprintf('%c',7),cG,0,0);
    [~,~,noresponse] = ...
        present_graphic_with_saccade_contingent_response(cG,1500,[],300,[0 0 50],0);
    if ~noresponse %fixated OK.
        ok = 1;
    else %perform drift correction
        Eyelink('StopRecording');
        sound(sin(0:100*pi/1000:100*pi)); %play beep to alert experimenter.
        calibrate_Eyetracker_cogent('d');
        Eyelink('StartRecording');
    end
end

if ok
    switch rulecode
        case 1, preparestring(sprintf('%c %c',43),cG,-10,0);cgdraw(10,-15,10,15);cgdraw(10,15,5,10);cgdraw(10,15,15,10);
        case 2, preparestring(sprintf('%c %c',88),cG,-10,0);cgdraw(10,-15,10,15);cgdraw(10,15,5,10);cgdraw(10,15,15,10);
        case 3, preparestring(sprintf('%c %c',43),cG,-10,0);cgdraw(10,-15,10,15);cgdraw(10,-15,5,-10);cgdraw(10,-15,15,-10);
        case 4, preparestring(sprintf('%c %c',88),cG,-10,0);cgdraw(10,-15,10,15);cgdraw(10,-15,5,-10);cgdraw(10,-15,15,-10);
    end
    present_graphic(cG,500);
    
    preparestring(sprintf('%c',7),cG,0,0);
    Eyelink('message','TRIALID %d %d %d',trind,1,0);
    timelog(end+1) = present_graphic(cG,500,scanning,PCs.fixation,'fixation');
end

%% 2. present decision phase, until decision is made (or timeout occurs)

chosen = 0;
noresponse = 0;
sumcost = 0;
turndur = nan(nT,1);

t = 1; %current turn

while ~chosen&~noresponse
    cG = 1;cgsetsprite(cG); %current graphic
    
    %a. draw picture
    for p = 1:numel(uncovered)
        if uncovered(p)                 %draw uncovered stimulus at current location
            loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
        elseif isnan(cost(t,p))         %picture is not available
            loadpict_mod(unavailstim,cG,loc(1,p),loc(2,p));
        else                            %picture is covered and available
            loadpict_mod(availstim,cG,loc(1,p),loc(2,p));
            if exist('coststim','var')    %overlay cost stimulus?
                loadpict_mod(coststim{t,p},cG,loc(1,p),loc(2,p));
            elseif WRITECOSTS
                cgrect(loc(1,p),loc(2,p),80,40,[0 0 0]);
                setforecolour(1,0,0);
                preparestring(sprintf('%0.0fp',cost(t,p)),cG,loc(1,p),loc(2,p));
                setforecolour(1,1,1);
            end
        end
    end
    
    %b. calculate permissable responses
    if t<=nT
        availpic = squeeze(~isnan(cost(t,:,:)))&~uncovered; %1 if available to uncover on this turn, 0 if not
        if canrespond(t) %allowed to make choice, as well as uncover, on this turn
            hotkeys = optkey;
        else
            hotkeys = [];
        end
    else %all information has been uncovered - final turn
        availpic = zeros(nO,nA);
        hotkeys = optkey;
    end
    
    gazelocs = [loc(:,find(availpic))' repmat(150,sum(availpic(:)),1)];
    pickey = []; %from previous version of experiment.
    %c. present picture and await response
    Eyelink('message','TRIALID %d %d %d',trind,2,t);
    [choseopt,turndur(t),noresponse,response_modality,timelog(end+1)] = present_graphic_with_saccade_contingent_response...
        (cG,choicetime(t),hotkeys,dwelltime,gazelocs,DRAW_DOTS,scanning,PCs.choice,'choice');
        
    %d. calculate what to do next
    if response_modality==1         %UNCOVERED PICTURE WITH SACCADE... 
        tmp = find(availpic);
        chose_to_reveal(t) = tmp(choseopt);
        uncovered(chose_to_reveal(t)) =1;%turn over picture in next turn
        sumcost = sumcost + cost(t,chose_to_reveal(t));
        t = t+1;
        
        % REDRAW PICTURE AND WAIT
        if dwelltime2 > 0
            for p = 1:numel(uncovered)
                if uncovered(p)                 %draw uncovered stimulus at current location
                    loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
                else isnan(cost(t,p))         %picture is not available
                    loadpict_mod(unavailstim,cG,loc(1,p),loc(2,p));
                end
                
            end
            gazelocs = gazelocs(choseopt,:);
            Eyelink('message','TRIALID %d %d %d',trind,21,t);
            [choseopt,~,noresponse,response_modality,timelog(end+1)] = present_graphic_with_saccade_contingent_response...
                (cG,choicetime(t),hotkeys,dwelltime2,gazelocs,DRAW_DOTS,scanning,PCs.choice+chose_to_reveal(t-1),sprintf('saccade %d',chose_to_reveal(t-1)));
        end
    end
    
    if response_modality==2     %PRESSED BUTTON TO SELECT OPTION...
        if any(choseopt==optkey)                %MADE CHOICE...
            chose_to_reveal(t) = nan;
            chosen=find(choseopt==optkey);          %break from loop
        elseif any(choseopt==pickey(:))         %UNCOVERED PICTURE... (shouldn't happen in this version)
            chose_to_reveal(t) = find(choseopt==pickey);
            uncovered(choseopt==pickey(:))=1;       %turn over picture in next turn
            sumcost = sumcost + cost(t,find(choseopt==pickey(:)));%add cost to sumcost
            t=t+1;                                  %move onto next turn
        elseif ~noresponse                      %MADE UNIDENTIFIED RESPONSE
            error('bug in code - response should either be noresponse, chose option, or turned over picture');
        end
    end
end

%% 3. present selected option

if selecttime>0&&~noresponse
    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(uncovered)
        if uncovered(p)                 %draw uncovered stimulus at current location
            loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
        else
            loadpict_mod(unavailstim,cG,loc(1,p),loc(2,p));
        end
    end
    Eyelink('message','TRIALID %d %d %d',trind,3,0);
    timelog(end+1) = present_graphic(cG,selecttime,scanning,PCs.select+chosen,sprintf('select %d',chosen));
end

if uncovertime>0&&~noresponse
    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(stim)
        loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
    end
    Eyelink('message','TRIALID %d %d %d',trind,31,0);
    timelog(end+1) = present_graphic(cG,uncovertime,scanning,PCs.uncover,'uncover');
end

%% 4. do rule-based calculations and present, if required

%calculate values
switch rule(1:3)
    case 'add'
        for o = 1:nO;
            rulestr = '+';
            ruleout{o} = ['= ' num2str(sum(value(o,:)))];
        end
    case 'mul'
        for o = 1:nO;
            rulestr = 'X';
            ruleout{o} = ['= ' num2str(prod(value(o,:)))];
        end
end

%calculate reward strings
rewstr = cell(nO,1);
for o = 1:nO
    if isnan(reward(o))
        rewstr{o} = 'Quits';
    else
        rewstr{o} = sprintf('%0.0fp',reward(o));
    end
end

%present
if ruletime>0&~noresponse
    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(stim)
        loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
    end
    for o = 1:nO
        preparestring(rulestr,cG,rulestrloc(1,o),rulestrloc(2,o));
    end
    timelog(end+1) = present_graphic(cG,ruletime*0.3,scanning,PCs.rule,'rule1');

    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(stim)
        loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
    end
    for o = 1:nO
        preparestring(rulestr,cG,rulestrloc(1,o),rulestrloc(2,o));
    end
    for o = 1:nO
        preparestring(ruleout{o},cG,ruleloc(1,o),ruleloc(2,o));
%         if (o==chosen||DISPLAY_ALL_FB)
%             if isnan(reward(o))
%                 setforecolour(1,1,0);
%             elseif reward(o)>0
%                 setforecolour(0,1,0);
%             elseif reward(o)<0
%                 setforecolour(1,0,0);
%             end
%             settextstyle('Arial',24);
%             preparestring(rewstr{o},cG,rewloc(1,o),rewloc(2,o));
%             settextstyle('Arial',40);
%             setforecolour(1,1,1);
%         end
    end
    
    Eyelink('message','TRIALID %d %d %d',trind,4,0);
    timelog(end+1) = present_graphic(cG,ruletime*0.7,scanning,PCs.rule,'rule2');
end

%% 5. calculate outcome and present, if required

%calculate outcome
if ~noresponse
    if isnan(reward(chosen))
        coststr = '';
    else
        coststr = sprintf('You paid %0.0fp',sumcost);
    end
    rewout = reward(chosen)
    outcome = rewout-sumcost;
    if isnan(reward(chosen))
        outstr = '';OScol = [0 0 0];
    elseif outcome>=0
        outstr = sprintf('Overall gain: �%0.2f',outcome/100); OScol = [0 1 0];
    else
        outstr = sprintf('Overall loss: �%0.2f',outcome/100); OScol = [1 0 0];
    end
else
    outcome = nan;
end

if ~noresponse
    cG = 1;cgsetsprite(cG);
    cgrect(selr(1,chosen),selr(2,chosen),selr(3,chosen),selr(4,chosen),CHOSECOLOUR);  %chosen card background
    for p = 1:numel(stim) %uncovered stimuli
        loadpict_mod(stim{p},cG,loc(1,p),loc(2,p));
    end
    if exist('rewstim','var')
        for r = 1:numel(rewstim)
            if ~isempty(rewstim{r})
                loadpict_mod(rewstim{r},cG,selr(1,r),selr(2,r));
            end
        end
    end
    for o = 1:nO
        preparestring(rulestr,cG,rulestrloc(1,o),rulestrloc(2,o));
    end
    for o = 1:nO
        preparestring(ruleout{o},cG,ruleloc(1,o),ruleloc(2,o));
        if (o==chosen||DISPLAY_ALL_FB)
            if isnan(reward(o))
                setforecolour(1,1,0);
            elseif reward(o)>0
                setforecolour(0,1,0);
            elseif reward(o)<0
                setforecolour(1,0,0);
            end
            settextstyle('Arial',24);
            preparestring(rewstr{o},cG,rewloc(1,o),rewloc(2,o));
            settextstyle('Arial',40);
            setforecolour(1,1,1);
        end
    end
    settextstyle('Arial',30)
    setforecolour(1,0,0);preparestring(coststr,cG,0,15);
    setforecolour(OScol(1),OScol(2),OScol(3));preparestring(outstr,cG,0,-15);setforecolour(1,1,1);
    settextstyle('Arial',40)
    Eyelink('message','TRIALID %d %d %d',trind,5,0);
    timelog(end+1) = present_graphic(cG,feedbacktime,scanning,PCs.feedback,'feedback');
end

%% 6. present hurry up screen, if subject didn't respond
if noresponse
    chosen = nan;
    cG = 2;cgsetsprite(cG);

    settextstyle('Arial',40);preparestring('Too slow!',cG);
    Eyelink('message','TRIALID %d %d %d',trind,6,0);
    present_graphic(cG,1000);
end


end %end of main function



%% STIMULUS PRESENTATION FUNCTION
function timelog = present_graphic(cG,presdur,scanning,portcode,eventname)


if 0 %for photodiode testing
    global flip_screen;
    
    if isempty(flip_screen)||flip_screen==1
        cgrect(0,-320,100,100,[1 1 1]);
        flip_screen=0;
    elseif flip_screen==0
        cgrect(0,-320,100,100,[0.2 0.2 0.2]);
        flip_screen=1;
    end
end

drawpict(cG);
if nargin>2
    if scanning;
        outportb(888,portcode);
    end
    timelog.time = time;
    timelog.code  = portcode;
    timelog.event = eventname;
    if scanning;
        wait(10); outportb(888,0);
        presdur = presdur - 10;
    end
else
    timelog = [];
end
wait(presdur);
clearpict(cG);

end


%% RESPONSE PRESENTATION FUNCTION
function [choseopt,RT,noresponse] = present_graphic_with_response(cG,tmax,hotkeys)

error('not using this function any more - see saccade contingent function instead.');
hkvalid = hotkeys(hotkeys~=0);

if 0 %for photodiode testing
    global flip_screen;
    
    if isempty(flip_screen)||flip_screen==1
        cgrect(0,-320,100,100,[1 1 1]);
        flip_screen=0;
    elseif flip_screen==0
        cgrect(0,-320,100,100,[0.2 0.2 0.2]);
        flip_screen=1;
    end
end

drawpict(cG);
clearpict(cG);

n=2;
tin = time;
while n>1 %sometimes waitkeydown will erroneously return two numbers rather than one.
    [choseopt,tout,n] = waitkeydown(tmax,hkvalid);
end
RT = tout - tin;

if isempty(choseopt) %no response
    noresponse = 1;
    choseopt = nan;
    RT = nan;
else
    noresponse = 0;
end
end

%% SACCADE-CONTINGENT RESPONSE PRESENTATION FUNCTION
function [choseopt,RT,noresponse,out_modality,timelog] = ...
    present_graphic_with_saccade_contingent_response(cG,tmax,hotkeys,dwelltime,gazelocs,draw_dots,scanning,portcode,eventname);

DRAW_TARGETS = 0;

hkvalid = hotkeys(hotkeys~=0);

if 0 %for photodiode testing
    global flip_screen;
    
    if isempty(flip_screen)||flip_screen==1
        cgrect(0,-320,100,100,[1 1 1]);
        flip_screen=0;
    elseif flip_screen==0
        cgrect(0,-320,100,100,[0.2 0.2 0.2]);
        flip_screen=1;
    end
end

drawpict(cG);
if nargin>6
    if scanning;
        outportb(888,portcode);
    end
    timelog.time = time;
    timelog.code  = portcode;
    timelog.event = eventname;
    if scanning;
        wait(10); outportb(888,0);
        tmax = tmax - 10;
    end
else
    timelog = [];
end

[choseopt,RT,out_modality] = gaze_resp_choice(gazelocs,hotkeys,dwelltime,tmax,cG,draw_dots,DRAW_TARGETS);

clearpict(cG);

if isnan(choseopt) %no response
    noresponse = 1;
else
    noresponse = 0;
end
end
