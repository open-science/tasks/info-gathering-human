function [x,y] = convert_EL_to_cogent(x,y);
%[x,y] = convert_EL_to_cogent(x,y);
%helper function, converts eyelink coordinates to cogent coordinates, as
%(0,0) is top-left of screen in EL coordinates

global cogent;

poss_res = [640 480;...
    800 600;...
    1024 768;...
    1152 864;...
    1280 1024;...
    1600 1200];
screen_res = cogent.display.res;
x0 = round(poss_res(screen_res,1)./2);
y0 = round(poss_res(screen_res,2)./2);
x = x-x0;
y = y0-y;