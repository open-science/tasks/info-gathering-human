%constants - check these before every run
CONDITION       = 5;        % 1 = addBIG; 2= multiplyBIG; 3 = addSMALL; 4 = multSMALL; 5 = interleaved
EYETRACKING     = 1;        % eyetracking?
EXPCONTROL      = 0;        % wait for experimenter input at various points in expt.?
DISPLAY         = 2;        % 0 for window, 1 for whole screen (see config_display, below)
NUM_TRIALS      = 200;
SCREEN_RES      = 3;        %(1=640x480, 2=800x600, 3=1024x768, 4=1152x864, 5=1280x1024, 6=1600x1200)


config_mouse;
config_keyboard;
config_display(DISPLAY,SCREEN_RES,[0 0 0],[1 1 1],'Arial',40,5); 

if eyelink( 'Initialize' ) ~= 0; return; end % open a connection to the eyetracker PC
Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');

start_cogent;
runET = 1;
while runET
    runET = calibrate_Eyetracker_cogent;
end
stop_cogent;