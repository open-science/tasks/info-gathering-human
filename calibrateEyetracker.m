function calibrateEyetracker(window,screenNumber)


dummymode=0;


%%%%%%%%%%
% STEP 2 %
%%%%%%%%%%

%[wW, wH] = WindowSize(window);

%%%%%%%%%%
% STEP 3 %
%%%%%%%%%%

% Provide Eyelink with details about the graphics environment
% and perform some initializations. The information is returned
% in a structure that also contains useful defaults
% and control codes (e.g. tracker state bit and Eyelink key values).

el=EyelinkInitDefaults(window);

el.backgroundcolour = BlackIndex(el.window);
el.msgfontcolour    = WhiteIndex(el.window);
el.imgtitlecolour = WhiteIndex(el.window);
el.targetbeep = 0;
el.calibrationtargetcolour= WhiteIndex(el.window);
el.calibrationtargetsize= 1;
el.calibrationtargetwidth=0.5;
el.displayCalResults = 1;
el.eyeimgsize=50;
EyelinkUpdateDefaults(el);

%%%%%%%%%%
% STEP 4 %
%%%%%%%%%%

% Initialization of the connection with the Eyelink Gazetracker.
% exit program if this fails.
if ~EyelinkInit(dummymode)
    fprintf('Eyelink Init aborted.\n');
    return;
end

[width, height]=Screen('WindowSize', screenNumber);


% make sure we're still connected.
if Eyelink('IsConnected')~=1 && ~dummymode
    fprintf('Not connected. exiting');
    cleanup;
    return;
end

%%%%%%%%%%
% STEP 5 %
%%%%%%%%%%

% SET UP TRACKER CONFIGURATION
% Setting the proper recording resolution, proper calibration type,
% as well as the data file content;

% it's location here is overridded by EyelinkDoTracker which resets it
% with display PC coordinates
Eyelink('command','screen_pixel_coords = %ld %ld %ld %ld', 0, 0, width-1, height-1);
Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, width-1, height-1);
% set calibration type.
Eyelink('command', 'calibration_type = HV5');
% you must send this command with value NO for custom calibration
% you must also reset it to YES for subsequent experiments
Eyelink('command', 'generate_default_targets = YES');

% STEP 5.1 modify calibration and validation target locations
Eyelink('command','calibration_samples = 6');
Eyelink('command','calibration_sequence = 0,1,2,3,4,5');
Eyelink('command','calibration_targets = %d,%d %d,%d %d,%d %d,%d %d,%d',...
    round(width/2),round(height/2), round(width/2),round(height*0.2),...
    round(  width/2),round(height - height*0.2), round( width*0.2),round(height/2),round(  width - width*0.2),round(height/2 ));
Eyelink('command','validation_samples = 5');
Eyelink('command','validation_sequence = 0,1,2,3,4,5');
Eyelink('command','validation_targets = %d,%d %d,%d %d,%d %d,%d %d,%d',...round(width/2),round(height/2), round(width/2),round(height*0.2),...
    round(  width/2),round(height - height*0.2), round( width*0.2), ...
round(height/2),round(  width - width*0.2),round(height/2 ));


% set parser (conservative saccade thresholds)
Eyelink('command', 'saccade_velocity_threshold = 35');
Eyelink('command', 'saccade_acceleration_threshold = 9500');
% set EDF file contents
% STEP 5.2 retrieve tracker version and tracker software version
[v,vs] = Eyelink('GetTrackerVersion');
fprintf('Running experiment on a ''%s'' tracker.\n', vs );
vsn = regexp(vs,'\d','match'); % wont work on EL I
if isempty(vsn)
    eyelinkI = 1;
else
    eyelinkI = 0;
end

if v == 3 && str2double(vsn{1}) == 4 % if EL 1000 and tracker version 4.xx
   
    % remote mode possible add HTARGET ( head target)
    Eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT');
    Eyelink('command', 'file_sample_data  = LEFT,RIGHT,GAZE,HREF,AREA,GAZERES,STATUS,INPUT,HTARGET');
    % set link data (used for gaze cursor)
    Eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,FIXUPDATE,INPUT');
    Eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,INPUT,HTARGET');
else
    Eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT');
    Eyelink('command', 'file_sample_data  = LEFT,RIGHT,GAZE,HREF,AREA,GAZERES,STATUS,INPUT');
    % set link data (used for gaze cursor)
    Eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,FIXUPDATE,INPUT');
    Eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,INPUT');
end
% allow to use the big button on the eyelink gamepad to accept the
% calibration/drift correction target
Eyelink('command', 'button_function 5 "accept_target_fixation"');


% STEP 5.3 change camera setup options
if v == 3
    % set pupil Tracking model in camera setup screen
    % no = centroid. yes = ellipse
    Eyelink('command', 'use_ellipse_fitter = no');
    % set sample rate in camera setup screen
    Eyelink('command', 'sample_rate = %d',1000);
end

% if desktop mount and tracker version is 4.2 or later change
% illumination
twropts = {'TOWER','MPRIM','MIRROR','BLRR','MLRR'};
% query tracker for mount type using elcl_select_configuration variable
[result,reply]=Eyelink('ReadFromTracker','elcl_select_configuration');

if ~eyelinkI && ~dummymode && ~result && ~any(strcmp(reply,twropts)) && str2double(vsn{1}) == 4 && str2double(vsn{2}) >= 2
    %set illumination power in camera setup screen
    % 1 = 100%, 2 = 75%, 3 = 50%
   
    Eyelink('command', 'elcl_tt_power = %d',2);
else
    disp('failed to change illumination. possible causes: DummyMode, EL not desktop mount, EL not 1000, EL version number pre 4.2, EL disconnected');
end

% query host to see if automatic calibration sequencing is enabled.
% ReadFromTracker needs to have 2 outputs.
% variables querable are listed in the .ini files in the host
% directories. Note that not all variables are querable.
[result, reply]=Eyelink('ReadFromTracker','enable_automatic_calibration');

if reply % reply = 1
    fprintf('Automatic sequencing ON');
else
    fprintf('Automatic sequencing OFF');
end

%%%%%%%%%%
% STEP 6 %
%%%%%%%%%%

% Hide the mouse cursor;
Screen('HideCursorHelper', window);
% enter Eyetracker camera setup mode, calibration and validation
EyelinkDoTrackerSetup(el);
%Eyelink('command', 'generate_default_targets = YES');

end